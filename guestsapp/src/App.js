import React, { Component, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import GuestList from './components/GuestList.js';
import SearchBox from './components/SearchBox';
import NumGuests from './components/NumGuests';
import GuestDetails from './components/GuestDetails';
import '../node_modules/font-awesome/css/font-awesome.min.css';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      guests: [],
      searchGuest: '',
      numGuest: 0,
      firstName: 'Name',
      lastName: 'Surname',
      company: 'company',
      mobileNumber: 'Mobile number',
      emailAddress: 'Email address (requested format is name@domain.com)',
      newMobileNumber: '',
      showInput: false
    }
    this.toggleInput = this.toggleInput.bind(this);
  }


  componentDidMount() {
    const xmlUrl = "https://gist.githubusercontent.com/eMatsiyana/e315b60a2930bb79e869b37a6ddf8ef1/raw/10c057b39a4dccbe39d3151be78c686dcd1101aa/guestlist.xml";
    fetch(xmlUrl)
      .then(response => response.text())
      .then(data => {
        let parser = new DOMParser();
        let xml = parser.parseFromString(data, "application/xml");
        let convert = require('xml-js');
        let result1 = convert.xml2json(data, {
          compact: true, spaces: 4
        })
        let jsonResults = JSON.parse(result1);
        let records = jsonResults.dataset.record;

        let arrayObjects = records;
        let newArray = [];
        arrayObjects.forEach(function (entry) {
          let firstName = entry.first_name._text;
          let lastname = entry.last_name._text;
          let company = entry.company._text;
          let mobileNumber = "012 234 5678";
          let emailAddress = firstName + "" + lastname + "@" + company + ".com";
          let newMobileNumber = "";
          let newEntry = {
            firstName: firstName,
            lastname: lastname,
            company: company,
            mobileNumber: mobileNumber,
            emailAddress: emailAddress,
            newMobileNumber: newMobileNumber
          };

          newArray.push(newEntry);
        })

        this.setState({ guests: newArray });
        localStorage.setItem("guests", JSON.stringify(this.state.guests));
        this.setState({ numGuest: newArray.length });
      })
  }

  handleSearch = (event) => {
    this.setState({ searchGuest: event.target.value });
  }

  listClick = (guest) => {
    console.log(JSON.stringify(guest.emailAddress));
    this.setState({
      firstName: guest.firstName,
      lastName: guest.lastname,
      company: guest.company,
      mobileNumber: guest.mobileNumber,
      emailAddress: guest.emailAddress,
      newMobileNumber: guest.newMobileNumber
    });
  }

  toggleInput() {
    this.setState({ showInput: true });
  }

  addItem = (event) => {
    event.preventDefault();
    const newNum = event.target.elements.newNumber.value;
    const newGuest = { newMobileNumber: newNum };
    this.setState({ guests: [...this.state.guests, newGuest] });
  }

  render() {

    let filterSearch = this.state.guests;
    //   let filterSearch = this.state.guests.filter((guest) => {
    //     return guest.firstName.includes(this.state.searchGuest) || guest.lastname.includes(this.state.searchGuest) ;
    //  })


    return (
      <div className="container">
        <div className="guest-wrapper">

          <SearchBox
            handleSearch={this.handleSearch}
          />

          <NumGuests numGuest={this.state.numGuest} />

          <GuestList
            listClick={this.listClick}
            filterSearch={filterSearch}
          />
        </div>

        <div className="guest-content">
          <GuestDetails
            firstName={this.state.firstName}
            lastname={this.state.lastName}
            company={this.state.company}
            emailAddress={this.state.emailAddress}
            mobileNumber={this.state.mobileNumber}
            addItem={this.addItem}
            toggleInput={this.toggleInput.bind(this)}
            showInput={this.state.showInput} />
        </div>
      </div>
    )
  }

}

export default App;
