import React, { Component } from 'react';
import Guest from './Guest';
import GuestDetails from './GuestDetails';


function GuestList(props) {

    let guests = props.filterSearch;

    return (
        <div>
            {
                guests.map((guest, i) => (
                    <Guest
                        key={i}
                        firstName={guest.firstName}
                        lastName={guest.lastname}
                        newMobileNumber={guest.newMobileNumber}
                        email={guest.email}
                        listClick={props.listClick.bind(this, guest)} />
                ))
            }
        </div>
    )
}

export default GuestList;