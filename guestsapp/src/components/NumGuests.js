import React, { Component } from 'react';

function NumGuests(props) {
    return (
        <div className="num-guests">
            <span>{props.numGuest} Confirmed guest</span>
        </div>
    )
}

export default NumGuests;