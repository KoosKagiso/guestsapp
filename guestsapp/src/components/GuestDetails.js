import React, { Component } from 'react';

function GuestDetails(props) {
    return (
        <div className="guest-card">
            <div className="guest-card-header">
                <div className="guest-image-wrapper">
                    <span className="guest-image"></span>
                </div>
                <div className="guest-info-wrapper">
                    <span className="guest-name">{props.firstName}</span>
                    <span className="guest-name">{props.lastname}</span>
                    <p className="guest-company">{props.company}</p>
                </div>
            </div>

            <div className="guest-card-form">
                <span>Contact Details:</span>
                <form action="" onSubmit={props.addItem}>
                    <input
                        type="text"
                        placeholder={props.emailAddress}
                        name="emailAddressInput"
                        required
                        pattern="[a-zA-Z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*"
                    />
                    <input
                        type="text"
                        placeholder={props.mobileNumber}
                        name="number"
                    />
                    <div className="btn-show">
                        <a className="btn-show-input" onClick={props.toggleInput}>
                            <i className="fa fa-plus-circle"></i>
                            Add number
                       </a>
                    </div>

                    {props.showInput ?
                        <div className="new-number">
                            <input
                                defaultValue="0"
                                type="text"
                                placeholder="New Number"
                                name="newNumber"
                                className=""
                            />

                            <button type="submit" className="btn-new-num"> <i className="fa fa-plus-circle"></i>&nbsp; Add new number</button>
                        </div> : null}
                </form>
            </div>
        </div>
    )
}

export default GuestDetails;