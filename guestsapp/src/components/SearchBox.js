import React, { Component } from 'react';


function SearchBox(props) {
    return (
        <div className="search">
            <div className="input-group">
                <input
                    onChange={props.handleSearch}
                    type="text"
                    placeholder="Search guestlist" />
            </div>
            <div className="input-group-append">
                <button className="btn btn-secondary" type="button">
                    <img src="../../assets/search2.png" alt="" />
                    <i className="fa fa-search">

                    </i>
                </button>
            </div>
        </div>
    )
}

export default SearchBox;