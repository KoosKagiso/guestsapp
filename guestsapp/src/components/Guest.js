import React, { Component } from 'react';


function Guest(props) {
    return (
        <div>
            <ul className="group-list">
                <li onClick={props.listClick} className="group-list-item">
                    <span className="image-icon"></span>
                    <span className="list-text">{props.firstName}</span>
                    <span className="list-text">{props.lastName}</span>
                    <span>{props.newMobileNumber}</span>
                </li>
            </ul>
        </div>
    )
}

export default Guest;