Gues App Challenge
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `Project Documented`

*componentDidMount*
 - this method fetches the xml data from the api provided.
 - i used npm install xml2json so i can use it convert my xml response to json
 - this method also makes new instance of new array where i did add email and mobile number
 - this method sets the new instance of array to my guests state

 *handleSearch*
 - this method handles search and gets the target value

 *listClick*
 - this method recives the guest object and pass the data to the detail section when ever each list item its clicked

 *toggleInput*
 - this method shows the hidden input so a user can enter their new mobile number

 *addItem*
 - the new mobile number is being added to the list with this method

*For Font icons*
- i have used the following npm install --save font-awesome
- i can just provide the class for the icons
